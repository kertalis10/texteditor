﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TextEditor
{
    public partial class SaveDBFileDialog : Form
    {
        public string FileName { get; private set;}

        public SaveDBFileDialog()
        {
            InitializeComponent();
        }

        private void SaveDBFileDialog_Load(object sender, EventArgs e)
        {

        }

        public DialogResult ShowDialog(string question, string fileName, string[] arrOfExt)
        {
            label.Text = question;
            textBox.Text = fileName;
            comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox.Items.AddRange(arrOfExt);
            comboBox.SelectedIndex = 0;

            return this.ShowDialog();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (textBox.Text.Trim() == "")
                label.ForeColor = Color.Red;
            else
            {
                FileName = textBox.Text.Trim() + comboBox.SelectedItem.ToString();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }       
    }
}
