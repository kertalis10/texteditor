﻿namespace TextEditor
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.openFromDBMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToDBMenuButton = new System.Windows.Forms.ToolStripMenuItem();
            this.workBox = new System.Windows.Forms.RichTextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFileMenuButton,
            this.saveFileMenuButton,
            this.openFromDBMenuButton,
            this.saveToDBMenuButton});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // openFileMenuButton
            // 
            this.openFileMenuButton.Name = "openFileMenuButton";
            this.openFileMenuButton.Size = new System.Drawing.Size(193, 22);
            this.openFileMenuButton.Text = "Открыть файл";
            this.openFileMenuButton.Click += new System.EventHandler(this.OpenFileMenuButton_Click);
            // 
            // saveFileMenuButton
            // 
            this.saveFileMenuButton.Name = "saveFileMenuButton";
            this.saveFileMenuButton.Size = new System.Drawing.Size(193, 22);
            this.saveFileMenuButton.Text = "Сохранить файл";
            this.saveFileMenuButton.Click += new System.EventHandler(this.SaveFileMenuButton_Click);
            // 
            // openFromDBMenuButton
            // 
            this.openFromDBMenuButton.Name = "openFromDBMenuButton";
            this.openFromDBMenuButton.Size = new System.Drawing.Size(193, 22);
            this.openFromDBMenuButton.Text = "Загрузить файл из БД";
            this.openFromDBMenuButton.Click += new System.EventHandler(this.OpenFromDBMenuButton_Click);
            // 
            // saveToDBMenuButton
            // 
            this.saveToDBMenuButton.Name = "saveToDBMenuButton";
            this.saveToDBMenuButton.Size = new System.Drawing.Size(193, 22);
            this.saveToDBMenuButton.Text = "Сохранить файл в БД";
            this.saveToDBMenuButton.Click += new System.EventHandler(this.SaveToDBMenuButton_Click);
            // 
            // workBox
            // 
            this.workBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.workBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.workBox.Location = new System.Drawing.Point(0, 27);
            this.workBox.Name = "workBox";
            this.workBox.Size = new System.Drawing.Size(800, 425);
            this.workBox.TabIndex = 1;
            this.workBox.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.workBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "TextEditor";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileMenuButton;
        private System.Windows.Forms.ToolStripMenuItem saveFileMenuButton;
        private System.Windows.Forms.ToolStripMenuItem openFromDBMenuButton;
        private System.Windows.Forms.ToolStripMenuItem saveToDBMenuButton;
        private System.Windows.Forms.RichTextBox workBox;
    }
}

