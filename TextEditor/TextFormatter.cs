﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TextEditor
{
    static class TextFormatter
    {
        //add highlite to xml
        public static void XMLFormat(RichTextBox box, string text)
        {
            box.Clear();
            bool isTitleStart = false;
            bool isKeyStart = false;
            string temp = null;

            for (int i = 0; i < text.Length; i++)
            {
                try
                {
                    if (text[i] == '<')
                    {
                        if (temp != null)
                        {
                            AppendText(box, temp, Color.Black);
                            temp = null;
                        }

                        if (text[i + 1] == '?')
                            isTitleStart = true;
                        else
                        {
                            isKeyStart = true;
                            AppendText(box, text[i].ToString(), Color.Blue);
                            i++;
                            if (text[i] == '/')
                            {
                                AppendText(box, text[i].ToString(), Color.Blue);
                                i++;
                            }
                        }

                    }
                    else if (text[i] == '>')
                    {
                        if (isTitleStart)
                        {
                            temp += text[i];
                            AppendText(box, temp, Color.Blue);
                            temp = null;
                            isTitleStart = false;
                            i++;
                        }
                        else if (isKeyStart)
                        {
                            AppendText(box, temp, Color.Red);
                            temp = null;
                            AppendText(box, text[i].ToString(), Color.Blue);
                            isKeyStart = false;
                            i++;
                        }
                    }

                    if(i < text.Length)
                        temp += text[i];
                }
                catch
                {
                    throw new Exception("Проблема с форматированием");
                }
            }           
        }

        //add highlite to json
        public static void JSONFormat(RichTextBox box, string text)
        {
            box.Clear();
            bool isStarted = false;
            string temp = null;


            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == '"' && !isStarted)
                {
                    isStarted = true;
                    if (temp != null)
                    {
                        AppendText(box, temp, Color.Black);
                        temp = null;
                    }
                }
                else if (text[i] == '"' && isStarted)
                {
                    isStarted = false;
                    temp += text[i];
                    i++;
                    if (text[i] == ':')
                    {
                        if (temp != null)
                        {
                            AppendText(box, temp, Color.DarkOrange);
                            temp = null;
                        }
                    }
                    else
                    {
                        if (temp != null)
                        {
                            AppendText(box, temp, Color.Red);
                            temp = null;
                        }
                    }

                }
               if(i < text.Length)
                    temp += text[i];
               if(i == text.Length - 1)
                    AppendText(box, temp, Color.Black);
            }
        }

        //add string with choosen color to richtextbox
        public static void AppendText(RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;
            box.SelectionColor = color;            
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }
    }
}
