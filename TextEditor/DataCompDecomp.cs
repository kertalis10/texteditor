﻿using System.IO;
using System.IO.Compression;
using System.Text;

namespace TextEditor
{
    class DataCompDecomp
    {
        //compress string to byte array
        public static byte[] Zip(string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);

            using (MemoryStream memoryStreamRead = new MemoryStream(bytes))
                using (MemoryStream memoryStreamWrite = new MemoryStream())
                {
                    using (GZipStream compStream = new GZipStream(memoryStreamWrite, CompressionMode.Compress))
                    {
                        memoryStreamRead.CopyTo(compStream);                   
                    }

                    return memoryStreamWrite.ToArray();
                }
        }

        //decompress byte array to string
        public static string Unzip(byte[] bytes)
        {
            using (MemoryStream memoryStreamRead = new MemoryStream(bytes))
                using (MemoryStream memoryStreamWrite = new MemoryStream())
                {
                    using (GZipStream decompStream = new GZipStream(memoryStreamRead, CompressionMode.Decompress))
                    {
                        decompStream.CopyTo(memoryStreamWrite);                    
                    }

                    return Encoding.UTF8.GetString(memoryStreamWrite.ToArray());
                }
        }

    }
}
