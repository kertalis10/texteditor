﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Windows.Forms;
using System.Configuration;

namespace TextEditor
{
    class DataClass
    {
        private readonly SQLiteConnection sqlConn;
        private readonly string connStr;
        private readonly string dbName;

        //file name in db
        public string FileName { get; private set; }

        public DataClass()
        {
            connStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;           
            sqlConn = new SQLiteConnection(connStr);
            dbName = ConfigurationManager.AppSettings["filename"];            
        }

        //Load file from db
        public string OpenFileFromDB()
        {   
            IsFileExists();

            if (!IsDTExists())                         
                throw new Exception("В БД нет записей");          

            string[] fileNames = GetFileNames(); //get file names from db

            if (fileNames.Length == 0)
                throw new Exception("В БД нет записей");
            else
            {
                OpenDBFileDialog ofd = new OpenDBFileDialog();
                if (ofd.ShowDialog("Выберите файл", fileNames) == DialogResult.OK)
                {
                    FileName = ofd.FileName;
                    return DataCompDecomp.Unzip( GetFile(ofd.FileName));                   
                }
                else
                    return null;
            }
        }

        //Save file to db
        public void SaveFileToDB(string fileName, string text)
        {
            if (!File.Exists(this.dbName))
            {
                CreateDB();  //create new db if file not exists
            }
            if (!IsDTExists())
            {
                AddDT(); //add new table to db if not exists
            }

            SaveDBFileDialog sfd = new SaveDBFileDialog();
            if (sfd.ShowDialog("Выберите имя файла и расширение", Path.GetFileNameWithoutExtension(fileName), new string[] { ".txt", ".xml", ".json" }) == DialogResult.OK)
            {
                if (IsEntryExists(sfd.FileName))
                {
                    DialogResult dr = MessageBox.Show("Такой файл уже есть в ДБ\nЗаменить?", "", MessageBoxButtons.YesNo);
                    if(dr == DialogResult.Yes)
                        UpdateRow(sfd.FileName, DataCompDecomp.Zip(text));
                }
                else
                {                    
                    InsertRow(sfd.FileName, DataCompDecomp.Zip(text));
                }
            }            
        }

        //check is db exists
        public void IsFileExists()
        {
            if (!File.Exists(dbName))
                throw new Exception("База данных не найдена\nПроверьте файл конфигурации");
        }

        //check is required table exists in db
        private bool IsDTExists()
        {
            try
            {
                sqlConn.Open();
                string sql = "SELECT count(*) FROM sqlite_master WHERE type='table' AND name='files';";               
                SQLiteCommand command = new SQLiteCommand(sql, sqlConn);                
                var count = command.ExecuteScalar();                
                if (Convert.ToInt32(count) == 1) return true;
                else return false;
            }
            catch
            {
                throw new Exception("Что-то пошло не так");
            }
            finally
            {
                sqlConn.Close();
            }            
        }

        //check is file already exists in table
        private bool IsEntryExists(string fileName)
        {
            try
            {
                sqlConn.Open();
                string sql = "SELECT count(*) FROM files WHERE FileName ='" + fileName + "';";
                SQLiteCommand command = new SQLiteCommand(sql, sqlConn);
                var count = command.ExecuteScalar();
                if (Convert.ToInt32(count) == 1) return true;
                else return false;
            }
            catch
            {
                throw new Exception("Что-то пошло не так");
            }
            finally
            {
                sqlConn.Close();
            }
        }

        //try to create db
        private void CreateDB()
        {
            try
            {
                SQLiteConnection.CreateFile(dbName);
            }
            catch
            {
                throw new Exception("Не удалось создать файл бд");
            }
        }

        //add a table to db
        private void AddDT()
        {
            try
            {
                sqlConn.Open();
                string sql = "CREATE TABLE files (FileName VARCHAR(50) UNIQUE, data BLOB)";
                SQLiteCommand command = new SQLiteCommand(sql, sqlConn);
                command.ExecuteNonQuery();
            }
            catch
            {
                throw new Exception("Не удалось создать таблицу");
            }
            finally
            {
                sqlConn.Close();
            }
        }

        //insert new row to db
        private void InsertRow(string filename, byte[] arr)
        {
            try
            {
                sqlConn.Open();
                string sql = @"INSERT INTO files VALUES (@FileName, @data)";
                SQLiteCommand command = new SQLiteCommand(sql, sqlConn);
                command.Parameters.Add("@FileName", System.Data.DbType.String);
                command.Parameters.Add("@data", System.Data.DbType.Binary);
                command.Parameters["@FileName"].Value = filename;
                command.Parameters["@data"].Value = arr;
                command.ExecuteNonQuery();
            }
            catch
            {
                throw new Exception("Не удалось сохранить файл в БД");
            }
            finally
            {
                sqlConn.Close();
            }
        }
        //update row in db
        private void UpdateRow(string filename, byte[] arr)
        {
            try
            {
                sqlConn.Open();
                string sql = @"UPDATE files SET data = @data WHERE FileName = @FileName";
                SQLiteCommand command = new SQLiteCommand(sql, sqlConn);
                command.Parameters.Add("@FileName", System.Data.DbType.String);
                command.Parameters.Add("@data", System.Data.DbType.Binary);
                command.Parameters["@FileName"].Value = filename;
                command.Parameters["@data"].Value = arr;
                command.ExecuteNonQuery();
            }
            catch
            {
                throw new Exception("Не удалось сохранить файл в БД");
            }
            finally
            {
                sqlConn.Close();
            }
        }

        //Get list of file name's from table
        private string[] GetFileNames()
        {
            List<string> fileNames = new List<string>();

            try
            {
                sqlConn.Open();
                string sql = "SELECT FileName FROM files;";
                SQLiteCommand command = new SQLiteCommand(sql, sqlConn);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    fileNames.Add(Convert.ToString(reader["FileName"]));

                return fileNames.ToArray();

            }
            catch
            {
                throw new Exception("Что-то пошло не так");
            }
            finally
            {
                sqlConn.Close();
            }            
        }

        //Read file from table
         private byte[] GetFile(string fileName)
         {
             byte[] buffer;

             try
             {
                 sqlConn.Open();
                 string sql = "SELECT data FROM files WHERE FileName='" + fileName + "';";
                 SQLiteCommand command = new SQLiteCommand(sql, sqlConn);                 
                 SQLiteDataReader reader = command.ExecuteReader();
                 while (reader.Read())
                 {
                     buffer = (byte[])reader["data"];
                     return buffer;
                 }

                 return null;
             }
             catch
             {
                 throw new Exception("Что-то пошло не так");
             }
             finally
             {
                 sqlConn.Close();
             }
         }          
    }
}
