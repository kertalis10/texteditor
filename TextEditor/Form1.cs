﻿using System;
using System.IO;
using System.Windows.Forms;

namespace TextEditor
{
    public partial class Form1 : Form
    {
        DataClass dc;
        string unformStr;
        string fileName;

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            dc = new DataClass();
        }

        //Open file from disk
        private void OpenFileMenuButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog
            {
                Filter = "text files(*.txt)|*.txt|xml files(*.xml)|*.xml|json files(*.json)|*json"
            };

            if (ofd.ShowDialog() == DialogResult.OK)
            {               
                fileName = Path.GetFileName(ofd.FileName);               
                string extention = Path.GetExtension(ofd.FileName);
                try
                {
                    using (StreamReader sr = new StreamReader(ofd.FileName))
                        unformStr = sr.ReadToEnd();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                workBox.Clear();
               
                if (Path.GetExtension(ofd.FileName) == ".xml")
                    TextFormatter.XMLFormat(workBox, unformStr);
                else if (Path.GetExtension(ofd.FileName) == ".json")
                    TextFormatter.JSONFormat(workBox, unformStr);
                else
                    workBox.Text = unformStr;
            }
        }

        //Save file to disk
        private void SaveFileMenuButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog
            {
                Filter = "text files(*.txt)|*.txt|xml files(*.xml)|*.xml|json files(*.json)|*json",
                FileName = fileName
            };

            if (sfd.ShowDialog() == DialogResult.OK)
            {                
                try
                {
                    File.WriteAllText(sfd.FileName, workBox.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        //Load file ftom db
        private void OpenFromDBMenuButton_Click(object sender, EventArgs e)
        {
            try
            {
                string unformStr = dc.OpenFileFromDB();
                if (unformStr != null)
                {
                    workBox.Clear();
                    if (Path.GetExtension(dc.FileName) == ".xml")
                        TextFormatter.XMLFormat(workBox, unformStr);
                    else if(Path.GetExtension(dc.FileName) == ".json")
                        TextFormatter.JSONFormat(workBox, unformStr);
                    else
                        workBox.Text = unformStr;
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //Save file to db
        private void SaveToDBMenuButton_Click(object sender, EventArgs e)
        {
            try
            {
                dc.SaveFileToDB(fileName, workBox.Text);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }        
    }
}
