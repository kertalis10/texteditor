﻿using System;
using System.Windows.Forms;

namespace TextEditor
{
    public partial class OpenDBFileDialog : Form
    {
        public string FileName { get; private set; }

        public OpenDBFileDialog()
        {
            InitializeComponent();
        }

        private void OpenDBFileDialog_Load(object sender, EventArgs e)
        {

        }
        public DialogResult ShowDialog(string question, string[] arrOfExt)
        {
            label.Text = question;
            comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox.Items.AddRange(arrOfExt);
            comboBox.SelectedIndex = 0;

            return this.ShowDialog();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            FileName = comboBox.SelectedItem.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
